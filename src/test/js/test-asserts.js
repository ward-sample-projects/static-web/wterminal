{
  const term = getWTerminal();
  term.printLn("Testing Asserts");
  //isTrue testing
  Asserts.runTest(`Asserts.isTrue(true)`,
    () => {
      try {
        Asserts.isTrue(true);
      } catch (e) {
        throw new Error("Assserts.isTrue(true) failed", e);
      }
    }, term);
  Asserts.runTest(`Asserts.isTrue(false)`,
    () => {
      let throwsNoError = true;
      try {
        Asserts.isTrue(false);
      } catch (e) {
        throwsNoError = false;
      }
      if (throwsNoError) throw new Error("Assert.isTrue(false) does not throw Error");
    }, term);
  // Asserts.runTest('This test should fail', ()=>{throw new Error("To fail test")}, term);
  //isFalse testing
  Asserts.runTest(`Asserts.isFalse(false)`,
    () => {
      try {
        Asserts.isFalse(false);
      } catch (e) {
        throw new Error("Assserts.isFalse(false) failed", e);
      }
    }, term);
  Asserts.runTest(`Asserts.isFalse(true)`,
    () => {
      let throwsNoError = true;
      try {
        Asserts.isFalse(true);
      } catch (e) {
        throwsNoError = false;
      }
      if (throwsNoError) throw new Error("Assert.isFalse(true) does not throw Error");
    }, term);
  //isEqual testing
  Asserts.runTest(`Asserts.isEqual(true, true)`,
    () => {
      try {
        Asserts.isEqual(true, true);
      } catch (e) {
        throw new Error("Assserts.isEqual(true, true) failed", e);
      }
    }, term);
  Asserts.runTest(`Asserts.isEqual(true, false)`,
    () => {
      let throwsNoError = true;
      try {
        Asserts.isEqual(true, false);
      } catch (e) {
        throwsNoError = false;
      }
      if (throwsNoError) throw new Error("Assert.isEqual(true, false) does not throw Error");
    }, term);
  //_equals testing
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(null, 0))`,
    () => Asserts.isFalse(Asserts._equals(null, 0)), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(null, undefined))`,
    () => Asserts.isFalse(Asserts._equals(null, undefined)), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(false, true))`,
    () => Asserts.isFalse(Asserts._equals(false, true)), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(false, 0))`,
    () => Asserts.isFalse(Asserts._equals(false, 0)), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals(true, true))`,
    () => Asserts.isTrue(Asserts._equals(true, true)), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(true, 1))`,
    () => Asserts.isFalse(Asserts._equals(true, 1)), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(0, '0'))`,
    () => Asserts.isFalse(Asserts._equals(0, '0')), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(0, 1))`,
    () => Asserts.isFalse(Asserts._equals(0, 1)), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals(1, 1))`,
    () => Asserts.isTrue(Asserts._equals(1, 1)), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(1, '1'))`,
    () => Asserts.isFalse(Asserts._equals(1, '1')), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals(Math.PI, Math.PI))`,
    () => Asserts.isTrue(Asserts._equals(Math.PI, Math.PI)), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals("Hello", "Hello"))`,
    () => Asserts.isTrue(Asserts._equals("Hello", "Hello")), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals("Hello", new String("Hello")))`,
    () => Asserts.isTrue(Asserts._equals("Hello", new String("Hello"))), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals("Hello", "hello"))`,
    () => Asserts.isFalse(Asserts._equals("Hello", "hello")), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals(["Hello", 1], ["Hello", 1]))`,
    () => Asserts.isTrue(Asserts._equals(["Hello", 1], ["Hello", 1])), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals(["Hello", 1], [1, "Hello"]))`,
    () => Asserts.isFalse(Asserts._equals(["Hello", 1], [1, "Hello"])), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals({}, {}))`,
    () => Asserts.isTrue(Asserts._equals({}, {})), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals({ a: "Hello", b: 1 }, { a: "Hello", b: 1 }))`,
    () => Asserts.isTrue(Asserts._equals({ a: "Hello", b: 1 }, { a: "Hello", b: 1 })), term);
  Asserts.runTest(`Asserts.isTrue(Asserts._equals({ a: "Hello", b: 1 }, { b: 1, a: "Hello" }))`,
    () => Asserts.isTrue(Asserts._equals({ a: "Hello", b: 1 }, { b: 1, a: "Hello" })), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals({ a: "Hello", b: 1 }, { b: 1, a: "Hello", c: undefined }))`,
    () => Asserts.isFalse(Asserts._equals({ a: "Hello", b: 1 }, { b: 1, a: "Hello", c: undefined })), term);
  Asserts.runTest(`Asserts.isFalse(Asserts._equals({ a: "Hello", b: 1 }, { b: 1, a: "Hello", c: null }))`,
    () => Asserts.isFalse(Asserts._equals({ a: "Hello", b: 1 }, { b: 1, a: "Hello", c: null })), term);
  term.printLn("Asserts OK");
}
