{
  const term = getWTerminal();
  term.printLn("Testing WTerminal.splitToArguments");
  Asserts.runTest(`Asserts.isEqual(WTerminal.splitToArguments("hello world"), ["hello", "world"]);`,
    () => Asserts.isEqual(WTerminal.splitToArguments("hello world"), ["hello", "world"]), term);
  Asserts.runTest(`Asserts.isEqual(WTerminal.splitToArguments("hello 1"), ["hello", "1"])`,
    () => Asserts.isEqual(WTerminal.splitToArguments("hello 1"), ["hello", "1"]), term);
  Asserts.runTest(`Asserts.isEqual(WTerminal.splitToArguments('"hello world" 1'), ["hello world", "1"])`,
    () => Asserts.isEqual(WTerminal.splitToArguments('"hello world" 1'), ["hello world", "1"]), term);
  Asserts.runTest(`Asserts.isEqual(WTerminal.splitToArguments("'hello world' 1"), ["hello world", "1"])`,
    () => Asserts.isEqual(WTerminal.splitToArguments("'hello world' 1"), ["hello world", "1"]), term);
  // Asserts.runTest('This test should fail', ()=>{throw new Error("To fail test")}, term);
  Asserts.runTest(`Asserts.isEqual(WTerminal.splitToArguments("\`hello world\` 1"), ["hello world", "1"])`,
    () => Asserts.isEqual(WTerminal.splitToArguments("`hello world` 1"), ["hello world", "1"]), term);
  Asserts.runTest(`Asserts.isEqual(WTerminal.splitToArguments("-rf /css/test.css myKitty.js 'my name with spaces'"),
    ["-rf", "/css/test.css", "myKitty.js", "my name with spaces"])`,
    () => Asserts.isEqual(WTerminal.splitToArguments("-rf /css/test.css myKitty.js 'my name with spaces'"),
      ["-rf", "/css/test.css", "myKitty.js", "my name with spaces"]), term);
  term.printLn("WTerminal.splitToArguments OK");
}
