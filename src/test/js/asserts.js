/* Author: Ward Truyen
* Version: 1.0.0
* About:   A tool for testing purposes
*/

class Asserts {
  static runTest(name, testFunction, term, pre = " ", printStacktraceAndStop = true) {
    term.print(pre+name);
    try {
      testFunction();
      term.printLn(WTerminal.createElement("span", {style:"color: green;"}, " OK"));
    } catch (e) {
      term.printError(" failed: " + e.toString());
      if(printStacktraceAndStop){
        term.printLn(WTerminal.createElement("span", {style:"color: blue;"}, e.stack));
        throw e;
      }
    }
  }

  /* Throws error when not true
   */
  static isTrue(value) {
    if (value === true) return;
    throw new Error('assert isTrue failed');
  }

  /* Throws error when not false
   */
  static isFalse(value) {
    if (value === false) return;
    throw new Error('assert isFalse failed');
  }
  /* Throws error when not equal
   */
  static isEqual(value1, value2) {
    if (Asserts._equals(value1, value2)) return;
    // if( typeof value1 === "object" || typeof value2 === "object") throw new Error('assert isEqual failed');
    throw new Error('assert isEqual failed\n' + value1 + " != " + value2);
  }

  /* returns true when values/objects v1 and v2 are equal
   */
  static _equals(v1, v2, _depth = 0) {
    if (v1 === v2) return true; // easy peasy
    let t = typeof v1
    if (t === "string" || v1 instanceof String && typeof v2 === "string" || v2 instanceof String) return v1.valueOf() === v2.valueOf(); // "Hello" equals to new String("Hello")
    if (t !== typeof v2 || v1.constructor.name !== v2.constructor.name) return false; // must be of identical type
    t += " " + v1.constructor.name;
    switch (t) {
      default: // Yet Unkown when i coded this?
        throw new Error("Todo: checking of type " + t);
      case 'boolean Boolean':
      case 'number Number':
      case 'object Date':
        return v1.valueOf() === v2.valueOf();
      case 'object Array':
        if (_depth > 4) throw Error("too deep");
        if (v1.length !== v2.length) return false;
        for (let i = 0; i < v1.length; i++) {
          if (Asserts._equals(v1[i], v2[i], _depth + 1) === false) return false;
        }
        return true;
      case 'object Object':
        if (_depth > 4) throw Error("too deep");
        const keys = Object.keys(v1);
        if (keys.length != Object.keys(v2).length) return false;
        for (let k in keys) {
          if (Asserts._equals(v1[k], v2[k], _depth + 1) === false) return false;
        }
        return true;
    }//-> switch (t)
  }//-> static equals(v1, v2)
}//-> class Asserts
