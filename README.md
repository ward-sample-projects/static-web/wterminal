```
*  _  .  _  _____ .----..----. .-.   .-..-..-. .-.  .--.  .-.   
* | |/ \| |[_   _]| {__ | {)  }| .`-'. ||~|| .`| | / {} \ | |   
* |  ,-,  |  | |  | {__ | .-. \| |\ /| || || |\  |/  /\  \| `--.
* '-'   `-'  '-'  `----'`-' `-'`-' ` `-'`-'`-' `-'`-'  `-'`----'
```

# For terminal fun on the web.

## About

WTerminal is written in JavaScript and CSS. For use in websites. No compiling needed. 1 big class.
This can be used as a dropdown terminal and as a terminal on the page(static), at the same time.
It provides easy tools for printing- and changing public variables(javascript) and cookies.
The terminal is useful for testing/logging-debugging without opening the console every time.
WTerminal instances can be augmented with extention-commands and extention-aliases without changing the terminal.js code.

## Screenshots

Sample terminal as a static element

![Screenshot static](public/screenshot1.png)

Sample terminal as a dropdown after playing ping pong

![Screenshot dropdown](public/screenshot2.png)

Sample terminal as a dropdown + darkmode

![Screenshot dropdown](public/screenshot5.png)

Terminal for testing

![Screenshot dropdown](public/screenshot3.png)

Terminal for testing + darkmode

![Screenshot dropdown](public/screenshot4.png)

Terminal as extention (surfing youtube)

![Screenshot dropdown](public/screenshot6.png)

## Testing

To test WTerminal I use serve on the src folder, [the npm package serve](https://www.npmjs.com/package/serve).
```
serve src/
```

To install serve i suggest the following command:
```
npm install --global serve
```

## Terminal as WebExtension

You can also use this terminal-project as a (Temporary) browser-extension. Get a terminal everywhere you surf.

How to documentation: 

- [MDN WebExtension](https://developer.mozilla.org/en-US/diocs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension)
- [firefox info](https://firefox-source-docs.mozilla.org/devtools-user/about_colon_debugging/index.html#this-firefox)
- [firefox about:debugging](about:debugging)
